# utils-txt

La clase TXT agrupa funcionalidades sobre un texto.

Listado de funcionalidades:
- startWith
- endWith
- equals
- compare
- contains
- lastPart
- firstPart
- toCamelCaseClass
- toCamelCaseVariable